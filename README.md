# IMUNO 2022 - Spheres

DO NOT RUN DIRECTLY FROM GOOGLE DRIVE !!!

Always copy to your local folder. The game will create local files.

## Prerequisities

- Python 3.9+

## Setup Linux

```
python3 -m venv venv
source venv/bin/activate
pip install numpy
pip install -r requirements.txt
```

## Setup Windows

1. create Python virtual environment
https://docs.python.org/3/library/venv.html

2. install modules 
```
pip install numpy 
pip install -r requirements.txt
```

## Keyboard mapping

escape - quit the game
- [ Q ] toggle fps info
- [ W ] toggle scene graph analyzer
- [ O ] show game overlay
- [ 1 ] select player 1
- [ 2 ] select player 2
- [ 3 ] select player 3
- [ 4 ] select player 4
- [ 5 ] select player 5
- [ 6 ] select player 6
- [ 7 ] select player 7
- [ 8 ] select player 8
- [ R ] reset selected player
- [ N ] nudge selected player toward the original position
- [ S ] scene reload
- [ L ] torch/light on
- Shift+[ L ] torch/light off
- [ T ] trail on
- Shift+[ T ] trail off