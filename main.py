import logging


import globals

logging.basicConfig(format="%(asctime)s: %(message)s", level=logging.INFO, datefmt="%H:%M:%S")

# TODO: this is indeed quite messy collection of calls

globals.game_screen.init()

globals.scene_config.reload()

globals.game_screen.keyboard_handler.on_reload()

globals.game_config_watcher.start()

globals.ctrlball_server.start()

logging.info("Running GameScreen")

globals.game_screen.run()
