import json
import logging
import socketserver
import sys
import threading
from json import JSONDecodeError

import globals

logger = logging.getLogger(__name__)


class SingleCtrlBallTCPHandler(socketserver.StreamRequestHandler):

    def __init__(self, request, client_address, server):
        self.ball = None
        self.lock = threading.Lock()
        self.send_queue = []
        super().__init__(request, client_address, server)

    def handle(self):
        logger.info('%s:connect', self.client_address)
        # self.request is the client connection

        try:
            while True:
                with self.lock:
                    send_queue_copy = self.send_queue
                    self.send_queue = []

                for msg in send_queue_copy:
                    self.send_back(msg)

                data = self.rfile.readline().strip()
                if not data:
                    break
                text = data.decode('utf-8')
                msg = json.loads(text)
                logger.debug("%s:receive,json:%s", self.client_address, msg)
                self.on_message(msg)

        except JSONDecodeError:
            logger.warning("%s:receive,raw:%s", self.client_address, text)
        finally:
            if self.ball is not None:
                with self.ball.lock:
                    self.ball.srv_set_connected(False)

            logger.info("%s:close", self.client_address)
            self.request.close()

    def on_message(self, message):
        event = message['event']
        logger.debug("%s:event:%s", self.client_address, event)
        if event == 'hello':
            ctrlball = message['ctrlball']
            time = message['time']
            with globals.ball_game.lock:
                self.ball = globals.ball_game.get_ball(ctrlball)

            with self.ball.lock:
                if self.ball.connected:
                    logger.warning("%s:event:%s:already connected", self.client_address, event)
                    raise Exception("CtrlBall already connected")
                self.ball.srv_on_hello(time, self.client_address)
                self.ball.srv_set_connected(True)
                self.ball.srv_send_back_callback(self.send_add_message)

        elif not self.ball:
            logger.warning("%s:event,illegal:%s", self.client_address, event)
        else:
            with self.ball.lock:
                self.ball.srv_handle_ctrl_message(message)

    def send_back(self, msg):
        json_data = json.dumps(msg)
        logger.info("%s:send_back:%s", self.client_address, json_data)
        self.request.sendall(bytes(json_data + "\n", "utf-8"))

    def send_add_message(self, msg):
        with self.lock:
            self.send_queue.append(msg)


class CtrlBallTcpServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    # Ctrl-C will cleanly kill all spawned threads
    daemon_threads = True
    # much faster rebinding
    allow_reuse_address = True

    def __init__(self, server_address):
        socketserver.TCPServer.__init__(self, server_address, SingleCtrlBallTCPHandler)
        self.server_thread = None


def server_function(hostname, port):
    logging.info("Starting CtrlBallServer %s:%d", hostname, port)
    server = CtrlBallTcpServer((hostname, port))
    # terminate with Ctrl-C
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        sys.exit(0)


class CtrlBallServer:

    def __init__(self):
        self.server_thread = None

    def start(self):
        hostname, port = '0.0.0.0', 12223
        self.server_thread = threading.Thread(target=server_function, args=(hostname, port), daemon=True)
        self.server_thread.start()

    def join(self):
        self.server_thread.join()
