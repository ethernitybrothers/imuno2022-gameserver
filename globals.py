from components.balls import BallGame
from components.configs import RuntimeConfig, SceneConfig, GameConfigWatcher
from components.scene import Scene
from components.screen import GameScreen
from server import CtrlBallServer

runtime_config = RuntimeConfig()

scene_config = SceneConfig()

ball_game = BallGame()

scene = Scene()

game_screen = GameScreen()

game_config_watcher = GameConfigWatcher()

ctrlball_server = CtrlBallServer()

