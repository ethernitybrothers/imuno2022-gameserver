import logging
import math
from threading import Lock

import numpy as np
from direct.actor.Actor import Actor
from panda3d.core import AmbientLight, PointLight

from scipy.spatial.transform import Rotation

import globals

logger = logging.getLogger(__name__)


class Vector3(list[float]):
    def __init__(self):
        self.last_timestamp = None
        self.extend([0, 0, 0])

    @property
    def x(self):
        return self[0]

    @x.setter
    def x(self, x):
        self[0] = x

    @property
    def y(self):
        return self[1]

    @y.setter
    def y(self, y):
        self[1] = y

    @property
    def z(self):
        return self[2]

    @z.setter
    def z(self, z):
        self[2] = z

    def set(self, x, y, z):
        self[0], self[1], self[2] = x, y, z

    def setv(self, vec):
        self[0], self[1], self[2] = vec[0], vec[1], vec[2]

    def mult(self, v):
        self[0] *= v[0]
        self[1] *= v[1]
        self[2] *= v[2]

    def add(self, v):
        self[0] += v[0]
        self[1] += v[1]
        self[2] += v[2]

    def update_xyz(self, timestamp, ax, ay, az):
        if self.last_timestamp is not None:
            time = timestamp - self.last_timestamp
            self.add([time*ax, time*ay, time*az])
        self.last_timestamp = timestamp

    def update(self, timestamp, a: list[float]):
        self.update_xyz(timestamp, a[0], a[1], a[2])

    def length(self):
        return math.sqrt(self.x**2 + self.y**2 + self.z**2)

    def distance_to(self, v: list[float]):
        return math.sqrt((self[0]-v[0])**2 + (self[1]-v[1])**2 + (self[2]-v[2])**2)

    def rotate_by(self, angles: list[float]):

        v = [self[0], self[1], self[2]]

        pitch, yaw, roll = angles[0], angles[2], angles[1]

        # rotate pitch - axis X
        ra = np.array([1, 0, 0])
        rv = pitch * ra
        rm = Rotation.from_rotvec(rv)
        v = rm.apply(v)

        # rotate yaw - axis Y
        ra = np.array([0, 1, 0])
        rv = yaw * ra
        rm = Rotation.from_rotvec(rv)
        v = rm.apply(v)

        # rotate roll - axis Z
        ra = np.array([0, 0, 1])
        rv = roll * ra
        rm = Rotation.from_rotvec(rv)
        v = rm.apply(v)

        return v[0], v[1], v[2]

    def copyFrom(self, v):
        self[0] = v[0]
        self[1] = v[1]
        self[2] = v[2]
        self.last_timestamp = v.last_timestamp


class SensorXYZ(Vector3):
    pass


def damp_one(value, d, min_value):
    new_value = value * (1 - d)
    if new_value < min_value:
        return 0
    else:
        return new_value


class Velocity(Vector3):

    def damp(self, factor, min_value):
        self.x = damp_one(self.x, factor, min_value)
        self.y = damp_one(self.y, factor, min_value)
        self.z = damp_one(self.z, factor, min_value)


class Position(Vector3):
    pass


class Orientation(Vector3):
    @property
    def yaw(self):
        return self.x

    @yaw.setter
    def yaw(self, x):
        self.x = x

    @property
    def pitch(self):
        return self.y

    @pitch.setter
    def pitch(self, y):
        self.y = y

    @property
    def roll(self):
        return self.z

    @roll.setter
    def roll(self, z):
        self.z = z

    @property
    def h(self):
        return self[0] * 90

    @property
    def p(self):
        return self[1] * 90

    @property
    def r(self):
        return self[2] * 90


class SceneObjectConfig:

    index = 0

    def __init__(self, doc, base_doc=None):
        self.doc = doc
        self.base_doc = base_doc
        self.name = self.fetch('name') or "object-{}".format(SceneObjectConfig.index)
        SceneObjectConfig.index += 1
        self.model = self.fetch('model')
        self.color = self.fetch('color')
        self.scale = self.fetch('scale')
        self.position = self.fetch('position')
        self.orientation = self.fetch('orientation')
        self.rotation = self.fetch('rotation')
        self.actor = self.fetch('actor')
        self.actor_multipart = self.fetch('actorMultipart')
        self.play_animations = self.fetch('playAnimations')
        self.light_enable = self.fetch('light.enable')
        self.light_color = self.fetch('light.color')
        self.light_attenuation = self.fetch('light.attenuation')
        self.light_shadow_cast = self.fetch('light.shadow.cast')
        self.light_shadow_buffer = self.fetch('light.shadow.buffer')

    def fetch(self, name):
        fields = name.split('.')
        walk = self.doc
        walk_base = self.base_doc
        for field in fields:
            if walk is None:
                walk = walk_base
            walk = walk.get(field)
            if walk_base:
                walk_base = walk_base.get(field)
        if walk is None:
            return walk_base
        else:
            return walk


class SceneObject:

    def __init__(self, name):
        self.lock = Lock()
        self.name = name
        self.do_reload_config = True
        self.do_unload = False
        self.config = None
        self.scale = Vector3()
        self.color = Vector3()
        self.rotation = Vector3()
        self.orientation = Orientation()
        self.velocity = Velocity()
        self.position = Position()
        self.shown = True
        self.node = None
        self.plnp = None

    def get_config(self):
        return globals.scene.object_configs[self.name]

    @property
    def render(self):
        return globals.game_screen.render

    @property
    def scene(self):
        return globals.scene

    def get_parent_node(self):
        return self.scene.node

    def unload(self):
        self.do_unload = False
        if self.node is not None:
            if self.plnp is not None:
                self.scene.node.clearLight(self.plnp)
                self.plnp.removeNode()
                self.plnp = None
            self.node.removeNode()
            self.node = None

    def load(self):
        self.do_reload_config = False
        self.unload()

        loader = globals.game_screen.loader
        self.config = self.get_config()
        if self.config.actor:
            if self.config.actor_multipart:
                model = loader.load_model(self.config.model)
                model.ls()
                parts = {}
                anims = {}
                characters = model.find_all_matches('**/+Character')
                characters_enum = enumerate(characters)
                for idx, np in characters_enum:
                    logger.info("Actor[%s], Multipart %d/%d: %s", self.config.name, idx+1, len(characters), np)
                    parts[np.get_name()] = np
                    anims[np.get_name()] = {}

                self.node = Actor(models=parts, anims=anims)
            else:
                self.node = Actor(self.config.model)
        else:
            self.node = loader.loadModel(self.config.model)

        parent_node = self.get_parent_node()
        self.node.reparentTo(parent_node)
        self.position.setv(self.config.position)
        self.orientation.setv(self.config.orientation)
        self.rotation.setv(self.config.rotation)
        self.velocity.set(0, 0, 0)
        self.color.setv(self.config.color)
        self.scale.setv(self.config.scale)

        for animation in self.config.play_animations:
            self.node.loop(animation)

        if self.config.light_enable:
            plight = PointLight('plight')
            light_color, light_attenuation = self.config.light_color, self.config.light_attenuation
            plight.setColor((light_color[0], light_color[1], light_color[2], 1))
            plight.setAttenuation((light_attenuation[0], light_attenuation[1], light_attenuation[2]))
            if self.config.light_shadow_cast:
                light_shadow_buffer = self.config.light_shadow_buffer
                plight.setShadowCaster(True, light_shadow_buffer[0], light_shadow_buffer[1])

            self.plnp = self.node.attachNewNode(plight)
            self.plnp.setPos(0, 0, 0)
            self.scene.node.setLight(self.plnp)

    def on_frame(self):
        with self.lock:
            if self.do_unload:
                self.unload()
            if self.do_reload_config:
                self.load()

            if self.node is None:
                return

            if self.shown:
                self.node.show()
            else:
                self.node.hide()
            self.node.setColor(self.color.x, self.color.y, self.color.z)
            self.node.setScale(self.scale.x, self.scale.y, self.scale.z)
            self.node.setPos(self.position.x, self.position.y, self.position.z)
            self.node.setHpr(self.orientation.h, self.orientation.p, self.orientation.r)
            self.orientation.x += self.rotation.x
            self.orientation.y += self.rotation.y
            self.orientation.z += self.rotation.z


class Scene:

    def __init__(self):
        self.lock = Lock()
        self.do_reload_config = True
        self.objects = {}
        self.object_configs = {}
        self.node = None
        self.alnp = None
        self.boundary_x = None
        self.boundary_y = None
        self.boundary_z = None
        self.boundary_bouncy = False

    @property
    def render(self):
        return globals.game_screen.render

    def on_frame(self):
        with self.lock:
            if self.do_reload_config:
                self.reload_config()
            objects = self.objects.copy()

        for obj in list(objects.values()):
            obj.on_frame()

    def reload_config(self):
        self.do_reload_config = False
        for o in self.objects.values():
            o.unload()
        logger.info("Reloading Scene")
        if self.node is not None:
            self.node.removeNode()
        self.node = self.render.attachNewNode('scene')

        scene_config_data = globals.scene_config.data
        cfg_objects = scene_config_data['objects']
        default_object_doc = next(x for x in cfg_objects if x['name'] == 'default')
        index = 0
        default_object_cfg = SceneObjectConfig(default_object_doc)
        self.object_configs['default'] = default_object_cfg
        logger.info("Added new object config: %s", default_object_cfg.name)
        for item in cfg_objects:
            index += 1
            name = item['name']
            if name != 'default':
                object_config = SceneObjectConfig(item, default_object_doc)
                self.object_configs[name] = object_config
                logger.info("Added new object config: %s", object_config.name)
                o = SceneObject(object_config.name)
                self.objects[object_config.name] = o

        cfg_scene = scene_config_data['scene']

        # TODO: not nice to do it here
        if self.alnp is not None:
            self.node.clearLight()
            self.alnp.removeNode()

        ambient_light = cfg_scene['ambientLight']
        alight = AmbientLight('alight')
        alight.setColor((ambient_light[0], ambient_light[1], ambient_light[2], 1))
        self.alnp = self.node.attachNewNode(alight)
        self.node.setLight(self.alnp)

        shader_auto = cfg_scene['shaderAuto']
        if shader_auto:
            self.node.setShaderAuto()

        cfg_boundary = cfg_scene['boundary']
        self.boundary_x = cfg_boundary['x']
        self.boundary_y = cfg_boundary['y']
        self.boundary_z = cfg_boundary['z']
