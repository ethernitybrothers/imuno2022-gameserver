from direct.gui.OnscreenText import OnscreenText
from panda3d.core import TextNode

import globals
from components.balls import Ball


class BallInfoCard:
    def __init__(self, overlay, ball: Ball):
        self.ball = ball
        self.node = OnscreenText(scale=0.05, align=TextNode.ALeft, font=overlay.font, frame=(1, 1, 1, 1))
        self.node.setPos(-0.5, 0.8-self.ball.index*0.4)
        self.node.setFg((1, 1, 1, 1))
        self.node.setBg((1, 1, 1, 0))
        self.node.setShadow((.5, .5, .5, 1))

    def update(self):
        s = []
        if self.ball.connected:
            if globals.game_screen.keyboard_handler.selected_player == self.ball.index:
                s.append(">= {} =<".format(self.ball.name))
            else:
                s.append(">- {} -<".format(self.ball.name))
            s.append("client_address: {}".format(self.ball.client_address))
            s.append("rotation: {:0.1f},{:0.1f},{:0.1f}".format(self.ball.orientation.yaw,
                                                                self.ball.orientation.pitch,
                                                                self.ball.orientation.roll))
            s.append("position: {:0.1f},{:0.1f},{:0.1f}".format(self.ball.position.x,
                                                                self.ball.position.y,
                                                                self.ball.position.z))
            s.append("velocity: {:0.1f},{:0.1f},{:0.1f}".format(self.ball.velocity.x,
                                                                self.ball.velocity.y,
                                                                self.ball.velocity.z))
            s.append("temperature: {:0.1f} °C".format(self.ball.temperature))
            s.append("battery: {} {}%".format(self.ball.battery.state, self.ball.battery.level))
        else:
            if globals.game_screen.keyboard_handler.selected_player == self.ball.index:
                s.append("== {} ==".format(self.ball.name))
            else:
                s.append("-- {} --".format(self.ball.name))
            s.append("disconnected")

        self.node.setText("\n".join(s))

    def set_visible(self, visible):
        if visible:
            self.node.show()
        else:
            self.node.hide()


class ExceptionCard:
    def __init__(self, overlay, name, e, slot):
        self.name = name
        self.slot = slot
        self.node = OnscreenText(scale=0.06, align=TextNode.ALeft, font=overlay.font, frame=(1, 1, 1, 1))
        self.node.setPos(-1.3, -0.7+slot*0.35)
        self.node.setFg((1, .5, .5, 1))
        self.node.setBg((1, 1, 1, 0))
        self.node.setShadow((.5, .5, .5, 1))
        self.update(e)

    def update(self, e):
        self.node.setText(self.name + "\n" + str(e))

    def destroy(self):
        self.node.destroy()


class Overlay:
    def __init__(self, medieval_app):
        self.medieval_app = medieval_app
        self.font = self.medieval_app.loader.loadFont('fonts/fantasquesansmono-regular.otf')
        self.font.setPixelsPerUnit(60)
        self.visible = globals.runtime_config.get('overlay-visible', True)
        self.cards = {}
        self.exception_cards = {}
        self.exception_card_slots = []

    def toggle(self):
        self.set_visible(not self.visible)

    def set_visible(self, visible):
        self.visible = visible
        for card in self.cards.values():
            card.set_visible(visible)
        if self.visible:
            globals.runtime_config.update('overlay-visible', True)
        else:
            globals.runtime_config.update('overlay-visible', False)

    def add_ball(self, ball: Ball):
        card = BallInfoCard(self, ball)
        self.cards[ball.name] = card
        card.update()
        card.set_visible(self.visible)
        return card

    def show_exception(self, name, e):
        if name in self.exception_cards:
            card = self.exception_cards[name]
            card.update(e)
        else:
            try:
                slot = self.exception_card_slots.index(False)
            except ValueError:
                self.exception_card_slots.append(False)
                slot = self.exception_card_slots.index(False)

            self.exception_card_slots[slot] = True
            card = ExceptionCard(self, name, e, slot)
            self.exception_cards[name] = card

    def clear_exception(self, name):
        if name in self.exception_cards:
            card = self.exception_cards.pop(name)
            self.exception_card_slots[card.slot] = False
            card.destroy()
