from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.task.TaskManagerGlobal import taskMgr
from panda3d.core import loadPrcFileData, GeoMipTerrain, DirectionalLight, AmbientLight, PointLight

from components.keyboard import KeyboardHandler
from components.overlay import Overlay

import globals
import gltf


class GameScreen(ShowBase):

    def __init__(self):
        config_data = open("run/spheres.prc", "r").read()
        loadPrcFileData('ctrlball', config_data)
        ShowBase.__init__(self)
        gltf.patch_loader(self.loader)
        self.overlay = None
        self.keyboard_handler = None
        self.setBackgroundColor(0, 0, 0)
        self.show_frame_rate = globals.runtime_config.get("show_frame_rate", True)
        self.scene_graph_analyzer = globals.runtime_config.get("scene_graph_analyzer", True)
        self.setFrameRateMeter(self.show_frame_rate)
        self.setSceneGraphAnalyzerMeter(self.scene_graph_analyzer)

        taskMgr.add(self.on_frame, 'GameScreen.on_frame')

    def init(self):
        self.overlay = Overlay(self)
        self.keyboard_handler = KeyboardHandler(self)

    def toggle_frame_rate(self):
        self.show_frame_rate = not self.show_frame_rate
        self.setFrameRateMeter(self.show_frame_rate)
        globals.runtime_config.update("show_frame_rate", self.show_frame_rate)

    def toggle_scene_graph_analyzer(self):
        self.scene_graph_analyzer = not self.scene_graph_analyzer
        self.setSceneGraphAnalyzerMeter(self.scene_graph_analyzer)
        globals.runtime_config.update("scene_graph_analyzer", self.scene_graph_analyzer)

    def on_frame(self, task):
        globals.scene.on_frame()
        globals.ball_game.on_frame()
        return Task.cont



