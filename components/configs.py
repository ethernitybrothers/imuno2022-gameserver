import json
import logging
import yaml

from os import listdir
from os.path import isfile, join

from pathlib import Path

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import globals

logger = logging.getLogger(__name__)

RUN_CONFIG_DIRECTORY = 'run'


class RuntimeConfig:

    def __init__(self):
        self.data = {}
        self.config_file = RUN_CONFIG_DIRECTORY + '/data.json'
        self.load()

    def update(self, key, value):
        self.data[key] = value
        self.save()

    def get(self, key, default):
        value = self.data.get(key)
        if value is not None:
            return value
        else:
            return default

    def save(self):
        json_string = json.dumps(self.data, indent=2, sort_keys=True)
        with open(self.config_file, 'w') as outfile:
            outfile.write(json_string)

    def load(self):
        path = Path(self.config_file)
        if path.is_file():
            with open(self.config_file, 'r') as infile:
                self.data = json.load(infile)


class SceneConfig:
    def __init__(self):
        self.data = {}

    def reload(self):
        logger.info("(Re)loading scene config from directory '%s'", RUN_CONFIG_DIRECTORY)
        try:
            globals.game_screen.overlay.clear_exception("SceneConfig")
            new_data = {}
            for f in listdir(RUN_CONFIG_DIRECTORY):
                if isfile(join(RUN_CONFIG_DIRECTORY, f)) and f.endswith('.yaml'):
                    doc = self.load_scene_config(f)
                    new_data |= doc
            logger.info("New scene config loaded - switching")
            self.data = new_data
            globals.ball_game.do_reload_config = True
            globals.scene.do_reload_config = True
        except Exception as e:
            logger.error("There is error in the config: %s", e)
            globals.game_screen.overlay.show_exception("SceneConfig", e)

    def load_scene_config(self, f):
        logger.info("Loading scene config file '%s'", f)
        stream = open(join(RUN_CONFIG_DIRECTORY, f), 'r')
        doc = yaml.load(stream, yaml.Loader)
        return doc


class GameConfigWatcher:

    def __init__(self):
        self.directory_to_watch = "run"
        self.observer = Observer()

    def start(self):

        event_handler = GameConfigWatcherHandler()
        self.observer.schedule(event_handler, self.directory_to_watch, recursive=True)
        self.observer.start()

    def join(self):
        self.observer.join()


class GameConfigWatcherHandler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):

        if event.is_directory:
            return None

        elif event.src_path.endswith('.yaml'):

            if event.event_type == 'created':
                logger.info("Config created - %s.", event.src_path)
                globals.scene_config.reload()

            elif event.event_type == 'modified':
                logger.info("Config modified - %s.", event.src_path)
                globals.scene_config.reload()

            elif event.event_type == 'deleted':
                logger.info("Config deleted - %s.", event.src_path)
                globals.scene_config.reload()
