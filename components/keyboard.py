import logging
import sys

from direct.interval.LerpInterval import LerpPosInterval, LerpHprInterval
from direct.showbase.DirectObject import DirectObject
from panda3d.core import WindowProperties, LVecBase3f

import globals


logger = logging.getLogger(__name__)


class KeyboardHandler(DirectObject):
    def __init__(self, medieval_app):
        self.medieval_app = medieval_app
        self.selected_player = 0
        # self.accept('enter', self.set_fullscreen)

    def on_reload(self):
        self.ignore_all()
        self.selected_player = 0
        self.accept('o', self.on_toggle_overlay)
        self.accept('escape', sys.exit)
        self.accept('q', self.on_toggle_frame_rate)
        self.accept('w', self.on_toggle_scene_graph_analyzer)
        self.accept('r', self.on_player_reset)
        self.accept('1', self.on_player_1_select)
        self.accept('2', self.on_player_2_select)
        self.accept('3', self.on_player_3_select)
        self.accept('4', self.on_player_4_select)
        self.accept('5', self.on_player_5_select)
        self.accept('6', self.on_player_6_select)
        self.accept('7', self.on_player_7_select)
        self.accept('8', self.on_player_8_select)
        self.accept('n', self.on_player_nudge)
        self.accept('s', self.on_scene_reload)
        self.accept('l', self.set_orb_torch, [True])
        self.accept('shift-l', self.set_orb_torch, [False])
        self.accept('t', self.set_ball_trail, [True])
        self.accept('shift-t', self.set_ball_trail, [False])
        cameras = globals.scene_config.data['cameras']

        for item in cameras:
            key, type = item['key'], item['type']
            if type == 'mouse':
                self.accept(key, self.on_camera_mouse)
            elif type == 'fix':
                position, orientation = item['position'], item['orientation']
                self.accept(key, self.on_camera_set, [position, orientation])
            elif type == 'log':
                self.accept(key, self.on_camera_log)

    def on_camera_mouse(self):
        logger.info("Setting camera to mouse")
        globals.game_screen.enableMouse()

    def on_camera_log(self):
        camera = globals.game_screen.camera
        pos = camera.getPos()
        hpr = camera.getHpr()
        logger.info("Current camera [%.1f, %.1f, %.1f] looking at [%.1f, %.1f, %.1f]",
                    pos[0], pos[1], pos[2], hpr[0], hpr[1], hpr[2])

    def on_camera_set(self, position, orientation):
        logger.info("Setting camera at %s oriented to %s", position, orientation)
        globals.game_screen.disableMouse()
        camera = globals.game_screen.camera
        new_pos = LVecBase3f(x=position[0], y=position[1], z=position[2])
        new_hpr = LVecBase3f(x=orientation[0], y=orientation[1], z=orientation[2])
        ip = LerpPosInterval(nodePath=camera, duration=3, pos=new_pos, blendType='easeInOut')
        rp = LerpHprInterval(nodePath=camera, duration=3, hpr=new_hpr, blendType='easeInOut')
        ip.start()
        rp.start()

    def on_toggle_overlay(self):
        self.medieval_app.overlay.toggle()

    def set_fullscreen(self):
        wp = WindowProperties()
        wp.set_fullscreen(True)
        self.medieval_app.win.requestProperties(wp)

    def on_toggle_frame_rate(self):
        self.medieval_app.toggle_frame_rate()

    def on_toggle_scene_graph_analyzer(self):
        self.medieval_app.toggle_scene_graph_analyzer()

    def on_player_reset(self):
        values = list(globals.ball_game.balls.values())
        if self.selected_player < len(values):
            logger.info("Resetting player %d", self.selected_player)
            values[self.selected_player].reset()

    def on_player_nudge(self):
        values = list(globals.ball_game.balls.values())
        if self.selected_player < len(values):
            logger.info("Player #%d: Nudge, nudge!", self.selected_player)
            values[self.selected_player].nudge()

    def select_player(self, n):
        self.selected_player = n
        logger.info("Selected player #%d", n)

    def on_player_1_select(self):
        self.select_player(0)

    def on_player_2_select(self):
        self.select_player(1)

    def on_player_3_select(self):
        self.select_player(2)

    def on_player_4_select(self):
        self.select_player(3)

    def on_player_5_select(self):
        self.select_player(4)

    def on_player_6_select(self):
        self.select_player(5)

    def on_player_7_select(self):
        self.select_player(6)

    def on_player_8_select(self):
        self.select_player(7)

    def on_scene_reload(self):
        self.on_reload()
        globals.ball_game.do_reload_config = True
        globals.scene.do_reload_config = True

    def set_orb_torch(self, value):
        values = list(globals.ball_game.balls.values())
        if self.selected_player < len(values):
            if value:
                logger.info("Player #%d: Let there be light!", self.selected_player)
            else:
                logger.info("Player #%d: Torch off", self.selected_player)
            values[self.selected_player].set_torch(value)

    def set_ball_trail(self, value):
        values = list(globals.ball_game.balls.values())
        if self.selected_player < len(values):
            if value:
                logger.info("Player #%d: create trail", self.selected_player)
            else:
                logger.info("Player #%d: trail off", self.selected_player)
            values[self.selected_player].set_trail(value)

