import logging
from threading import Lock

import numpy

import globals

from components.scene import SceneObjectConfig, SceneObject, Vector3, SensorXYZ, Position, Orientation

logger = logging.getLogger(__name__)


class BatteryState:
    def __init__(self):
        self.state = 'unkown'
        self.level = 1


class ActivityDetector:
    def __init__(self):
        self.buffer = []
        self.max_buffer = 50

    def add_value(self, v):
        self.buffer.append(v)
        if len(self.buffer) > self.max_buffer:
            self.buffer.pop(0)

    def is_active(self):
        for v in self.buffer:
            if v:
                return True
        return False


class TrailPiece:
    def __init__(self, trail, name, position: Position, orientation: Orientation):
        ball_config = trail.origin.config
        self.color = ball_config.trailColor
        self.animationRotation = ball_config.trailAnimationRotation
        self.animationScaleBy = ball_config.trailAnimationScaleBy
        parent_node = trail.get_parent_node()
        loader = globals.game_screen.loader
        self.node = loader.loadModel(trail.origin.config.trailModel)
        self.node.reparentTo(parent_node)
        self.scale = Vector3()
        self.scale.setv(ball_config.trailScale)
        self.position = Position()
        self.position.copyFrom(position)
        self.orientation = Orientation()
        self.orientation.copyFrom(orientation)
        self.update()

    def destroy(self):
        self.node.removeNode()

    def on_frame(self):
        self.scale.mult(self.animationScaleBy)
        self.orientation.add(self.animationRotation)
        self.update()

    def update(self):
        self.node.setPos(self.position.x, self.position.y, self.position.z)
        self.node.setColor(self.color[0], self.color[1], self.color[2])
        self.node.setHpr(self.orientation.h, self.orientation.p, self.orientation.r)
        self.node.setScale(self.scale.x, self.scale.y, self.scale.z)


class BallTrail:
    def __init__(self, origin):
        self.pieces = []
        self.origin = origin
        self.enable = False
        self.last_position = None
        self.n = 0

    def on_frame(self):
        for piece in self.pieces:
            piece.on_frame()

    def on_position_change(self, position: Position, orientation: Orientation):
        if not self.enable:
            return
        if self.last_position is None:
            self.last_position = Position()
        else:
            distance = self.last_position.distance_to(position)
            if distance > self.origin.config.trailSpacing:
                self.instantiate(position, orientation)
                self.last_position.copyFrom(position)

    def get_parent_node(self):
        return self.origin.scene.node

    def instantiate(self, position: Position, orientation: Orientation):
        name = 'trail -' + str(self.n)
        piece = TrailPiece(self, name, position, orientation)
        self.pieces.append(piece)
        self.n += 1
        if len(self.pieces) > self.origin.config.trailLength:
            piece = self.pieces.pop(0)
            piece.destroy()


class BallConfig(SceneObjectConfig):
    def __init__(self, doc, base=None):
        SceneObjectConfig.__init__(self, doc, base)
        self.motionBounce = self.fetch('motion.bounce')
        self.motionSensitivity = self.fetch('motion.sensitivity')
        self.velocityDamperFactor = self.fetch('motion.velocityDamper.factor')
        self.velocityDamperMinValue = self.fetch('motion.velocityDamper.minValue')
        self.stopDetectAccelarationMax = self.fetch('motion.stopDetector.maxAccelaration')
        self.orbColors = self.fetch('orb.colors')
        self.orbActiveColors = self.fetch('orb.activeColors')
        self.orbColorTransitionMillis = self.fetch('orb.colorTransitionMillis')
        self.orbTorch = self.fetch('orb.torch')
        self.trailEnable = self.fetch('trail.enable')
        self.trailModel = self.fetch('trail.model')
        self.trailColor = self.fetch('trail.color')
        self.trailScale = self.fetch('trail.scale')
        self.trailSpacing = self.fetch('trail.spacing')
        self.trailLength = self.fetch('trail.length')
        self.trailAnimationRotation = self.fetch('trail.animation.rotation')
        self.trailAnimationScaleBy = self.fetch('trail.animation.scaleBy')


class Ball(SceneObject):
    count = 0

    def __init__(self, name):
        SceneObject.__init__(self, name)
        self.lock = Lock()
        self.index = Ball.count
        Ball.count += 1
        self.connected = False
        self.client_address = None
        self.gyroscope = SensorXYZ()
        self.magnetometer = SensorXYZ()
        self.accelerometer = SensorXYZ()
        self.user_accelerometer = SensorXYZ()
        self.battery = BatteryState()
        self.temperature = 0.0
        self.activity_detector = ActivityDetector()
        self.overlayCard = globals.game_screen.overlay.add_ball(self)
        self.send_back_callback = None
        self.is_active = False
        self.trail = BallTrail(origin=self)
        logger.info("New ball created: %s", name)

    def on_frame(self):
        super().on_frame()
        self.trail.on_frame()
        with self.lock:
            self.overlayCard.update()

    def reset(self):
        self.do_unload = True
        self.do_reload_config = True

    def reset_timestamp(self, timestamp):
        if self.config is None:
            self.velocity.last_timestamp = None
            self.position.last_timestamp = None
        else:
            timestamp *= self.config.motionSensitivity

    def nudge(self):
        cp = self.position
        op = self.config.position
        vx = (op[0] - cp.x)
        vy = (op[1] - cp.y)
        vz = (op[2] - cp.z)
        self.velocity.set(vx, vy, vz)

    def srv_on_hello(self, timestamp, client_address):
        self.client_address = client_address
        self.do_reload_config = True
        self.reset_timestamp(timestamp)

    def srv_handle_ctrl_message(self, message):
        event = message['event']
        if event == 'orientation':
            self.on_event_orientation(message)
        elif event == 'accelerometer':
            self.on_event_accelerometer(message)
        elif event == 'user-accelerometer':
            self.on_event_user_accelerometer(message)
        elif event == 'gyroscope':
            self.on_event_gyroscope(message)
        elif event == 'magnetometer':
            self.on_event_magnetometer(message)
        elif event == 'temperature':
            self.on_event_temperature(message)
        elif event == 'battery':
            self.on_event_battery(message)

    def on_event_magnetometer(self, message):
        logger.debug("%s:magnetometer:%a", self.name, message)
        time, x, y, z = message['time'], message['x'], message['y'], message['z']
        self.magnetometer.x = x
        self.magnetometer.y = y
        self.magnetometer.z = z

    def on_event_gyroscope(self, message):
        logger.debug("%s:gyroscope:%a", self.name, message)
        time, x, y, z = message['time'], message['x'], message['y'], message['z']
        self.gyroscope.x = x
        self.gyroscope.y = y
        self.gyroscope.z = z

    def on_event_accelerometer(self, message):
        logger.debug("%s:accelerometer:%a", self.name, message)
        time, x, y, z = message['time'], message['x'], message['y'], message['z']
        self.accelerometer.x = x
        self.accelerometer.y = y
        self.accelerometer.z = z

    def on_event_user_accelerometer(self, message):
        logger.debug("%s:user-accelerometer:%a", self.name, message)
        time, x, y, z = message['time'], message['x'], message['y'], message['z']
        if self.config is None:
            return
        time *= self.config.motionSensitivity
        self.user_accelerometer.x = x
        self.user_accelerometer.y = y
        self.user_accelerometer.z = z
        rotation_vector = [self.orientation[0] + self.config.orientation[0],
                           self.orientation[1] + self.config.orientation[1],
                           self.orientation[2] + self.config.orientation[2]]
        absolute_acceleration = self.user_accelerometer.rotate_by(rotation_vector)
        # TODO: need to adjust to absolute earth-relative acceleration vectors
        # https://stackoverflow.com/questions/11578636/acceleration-from-devices-coordinate-system-into-absolute-coordinate-system
        self.velocity.update(time, absolute_acceleration)
        self.position.update(time, self.velocity)

        prev_active = self.is_active
        if self.user_accelerometer.length() < self.config.stopDetectAccelarationMax:
            self.activity_detector.add_value(False)
        else:
            self.activity_detector.add_value(True)

        self.is_active = self.activity_detector.is_active()
        if self.is_active:
            self.trail.on_position_change(self.position, self.orientation)

        if prev_active != self.is_active:
            if self.is_active:
                logger.info("%s:active", self.name)
                payload = {
                    "type": "orb",
                    "colors": self.config.orbActiveColors,
                    "colorTransitionMillis": self.config.orbColorTransitionMillis
                }
                self.srv_send_back(payload)
            else:
                logger.info("%s:resting", self.name)
                payload = {
                    "type": "orb",
                    "colors": self.config.orbColors,
                    "colorTransitionMillis": self.config.orbColorTransitionMillis
                }
                self.srv_send_back(payload)

        if not self.is_active:
            factor = self.config.velocityDamperFactor
            min_value = self.config.velocityDamperMinValue
            self.velocity.damp(factor, min_value)

        x, y, z = self.position.x, self.position.y, self.position.z
        scene = globals.scene
        if self.config.motionBounce:
            if x < scene.boundary_x[0] and self.velocity.x < 0:
                self.velocity.x = -self.velocity.x
            elif x > scene.boundary_x[1] and self.velocity.x > 0:
                self.velocity.x = -self.velocity.x
            if y < scene.boundary_y[0] and self.velocity.y < 0:
                self.velocity.y = -self.velocity.y
            elif y > scene.boundary_y[1] and self.velocity.y > 0:
                self.velocity.y = -self.velocity.y
            if z < scene.boundary_z[0] and self.velocity.z < 0:
                self.velocity.z = -self.velocity.z
            elif z > scene.boundary_z[1] and self.velocity.z > 0:
                self.velocity.z = -self.velocity.z
        else:
            self.position.x = numpy.clip(self.position.x, scene.boundary_x[0], scene.boundary_x[1])
            self.position.y = numpy.clip(self.position.y, scene.boundary_y[0], scene.boundary_y[1])
            self.position.z = numpy.clip(self.position.z, scene.boundary_z[0], scene.boundary_z[1])

    def on_event_orientation(self, message):
        logger.debug("%s:orientation:%a", self.name, message)
        time, yaw, pitch, roll = message['time'], message['yaw'], message['pitch'], message['roll']
        self.orientation.yaw = yaw
        self.orientation.pitch = pitch
        self.orientation.roll = roll

    def on_event_battery(self, message):
        logger.debug("%s:battery:%a", self.name, message)
        time, state, level = message['time'], message['state'], message['level']
        self.battery.state = state
        self.battery.level = level

    def on_event_temperature(self, message):
        logger.debug("%s:temperature:%a", self.name, message)
        time, temperature = message['time'], message['temperature']
        self.temperature = temperature

    def srv_set_connected(self, value: bool):
        self.connected = value
        if value:
            self.shown = True
            self.is_active = False
            logger.info("%s:connected", self.name)
        else:
            self.shown = False
            self.client_address = None
            self.do_unload = True
            logger.info("%s:disconnected", self.name)

    def get_config(self):
        return globals.ball_game.get_ball_config(self.name)

    def get_parent_node(self):
        return self.render

    def load(self):
        logger.info("Reloading config of ball '%s'", self.name)
        super().load()
        payload = {
            "type": "orb",
            "hapticFeedback": "success",
            "colors": self.config.orbColors,
            "colorTransitionMillis": self.config.orbColorTransitionMillis,
            "torch": self.config.orbTorch
        }
        self.srv_send_back(payload)
        self.trail.enable = self.config.trailEnable

    def set_torch(self, state):
        self.srv_send_back({"type": "orb", "torch": state})

    def set_trail(self, state):
        self.trail.enable = state

    def srv_send_back_callback(self, callback):
        self.send_back_callback = callback

    def srv_send_back(self, msg):
        if self.send_back_callback is not None:
            self.send_back_callback(msg)


class BallGame:

    def __init__(self):
        self.lock = Lock()
        self.do_reload_config = True
        self.balls = {}
        self.ball_configs = {}

    def on_frame(self):
        with self.lock:
            if self.do_reload_config:
                self.reload_config()
            balls = self.balls.copy()

        for ball in list(balls.values()):
            ball.on_frame()

    def get_ball(self, name) -> Ball:
        ball = self.balls.get(name)
        if not ball:
            ball = Ball(name)
            self.balls[name] = ball
        return ball

    def get_ball_config(self, name) -> BallConfig:
        ball_config = self.ball_configs.get(name)
        if not ball_config:
            ball_config = self.ball_configs.get('default')
        return ball_config

    def reload_config(self):
        self.do_reload_config = False
        logger.info("Reloading BallGame config")
        scene_config_data = globals.scene_config.data
        cfg_balls = scene_config_data['balls']
        default_ball_doc = next(x for x in cfg_balls if x['name'] == 'default')
        default_ball_cfg = BallConfig(default_ball_doc)
        logger.info("Added new ball config: %s", default_ball_cfg.name)

        new_ball_configs = { default_ball_cfg.name: default_ball_cfg }
        for item in cfg_balls:
            if item['name'] != 'default':
                ball_config = BallConfig(item, default_ball_doc)
                logger.info("Added new ball config: %s", ball_config.name)
                new_ball_configs[ball_config.name] = ball_config
        self.ball_configs = new_ball_configs
        for ball in self.balls.values():
            with ball.lock:
                ball.do_reload_config = True
